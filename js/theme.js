if (typeof jQuery === 'undefined') {
    throw new Error('Bootstrap\'s JavaScript requires jQuery')
} +
(function($) {

    var $event = $.event,
        $special,
        resizeTimeout;

    $special = $event.special.debouncedresize = {
        setup: function() {
            $(this).on("resize", $special.handler);
        },
        teardown: function() {
            $(this).off("resize", $special.handler);
        },
        handler: function(event, execAsap) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function() {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply(context, args);
                };

            if (resizeTimeout) {
                clearTimeout(resizeTimeout);
            }

            execAsap ?
                dispatch() :
                resizeTimeout = setTimeout(dispatch, $special.threshold);
        },
        threshold: 500
    };

})(jQuery); +
/*
 * Viewport - jQuery selectors for finding elements in viewport
 *
 * Copyright (c) 2008-2009 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *  http://www.appelsiini.net/projects/viewport
 *
 */
(function($) {


    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */

    $.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };


})(jQuery);
! function($) {
    "use strict";
    var Shift = function(element) {
        this.$element = $(element);
        this.$prev = this.$element.prev();
        !this.$prev.length && (this.$parent = this.$element.parent())
    }
    Shift.prototype = {
        constructor: Shift,
        init: function() {
            var $el = this.$element,
                method = $el.data()['toggle'].split(':')[1],
                $target = $el.data('target')
            $el.hasClass('in') || $el[method]($target).addClass('in')
        },
        reset: function() {
            this.$parent && this.$parent['prepend'](this.$element);
            !this.$parent && this.$element['insertAfter'](this.$prev)
            this.$element.removeClass('in');
        }
    }
    $.fn.shift = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('shift')
            if (!data) $this.data('shift', (data = new Shift(this)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.shift.Constructor = Shift
}(window.jQuery);
Date.now = Date.now || function() {
    return +new Date;
};
! function($) {
    $(function() {
        var is_touch_device = function() {
            return !!('ontouchstart' in window) || !!('onmsgesturechange' in window);
        };
        !is_touch_device() && $('html').addClass('no-touch');
        var $window = $(window);
        var mobile = function(option) {
            if (option == 'reset') {
                $('[data-toggle^="shift"]').shift('reset');
                return;
            }
            $('[data-toggle^="shift"]').shift('init');
        };
        var tablet = function(option){
            if (option == 'reset'){
                $('[data-toggle^="shiftTab"]').shift('reset');
                return;
            }
            $('[data-toggle^="shiftTab"]').shift('init');
        }
        $window.width() < 767 && mobile();
        $window.width() < 979 && tablet();
        var $resize, $width = $window.width();
        $window.resize(function() {
            if ($width !== $window.width()) {
                clearTimeout($resize);
                $resize = setTimeout(function() {
                    $window.width() < 767 && mobile();
                    $window.width() >= 767 && mobile('reset');
                    $window.width() < 979 && tablet();
                    $window.width() >= 979 && tablet('reset');
                    $width = $window.width();
                }, 500);
            }
        });
    });
}(window.jQuery);
(function($) {

    $(document).ready(function() {

        var w = $(window),
            d = $(document),
            logoWrapper = $('.logo-wrapper'),
            logoIMG = logoWrapper.find('img'),
            gridGallery = $('.grid-gallery'),
            gallerySource = gridGallery.find('#source'),
            galleryDestination = gridGallery.find('#destination'),
            galleryDestinationList = galleryDestination.find('li'),
            frmSelCountry = $('.selectcustom-country'),
            fuelDiv = $('.bas-panel'),
            fuelDivProgressBar = fuelDiv.find('#bar'),
            togglehvr = $('.togglehvr'),
            wizardReservation = $('#wizard-reservation'),
            wizardUlSteps = wizardReservation.find('ul.steps'),
            progressContainer = $('.progress-container'),
            openDateFrom = $("#open_date_from"), 
            openDateto = $("#open_date_to"),
            floatForm = $("#float-form"),
            contentFR = floatForm.find('.content-flform'),
            FRTopCol = floatForm.find('.top-col'),
            FRBottomCol = floatForm.find('.bottom-col'); 


        // Carousel
        if ($().owlCarousel) {

            var wrappCarousel = $(".wrap-carousel"),
                singleWrapCarousel = $('.wrap-single-carousel'),
                wrapReviewCarousel = $('.wrap-review-carousel');

            wrapReviewCarousel.each(function(index, el) {
                var $this = $(this),
                    colCarousel = $this.find('.owl-carousel'),
                    carouselNav = $this.find('.carousel-nav'),
                    owlReviews = $this.find("#owl-reviews");


                owlReviews.owlCarousel({
                    slideSpeed: 800,
                    paginationSpeed: 400,
                    singleItem: true,
                    pagination: false
                });

                var elPrevNavigation = $this.find('a.carousel-prev'),
                    elNextNavigation = $this.find('a.carousel-next');

                elNextNavigation.click(function(e) {
                    e.preventDefault();
                    colCarousel.trigger('owl.next');
                });

                elPrevNavigation.click(function(e) {
                    e.preventDefault();
                    colCarousel.trigger('owl.prev');
                });

            });

            // console.log(colCarousel);
            wrappCarousel.each(function(index, element) {
                var $this = $(this),
                    colCarousel = $this.find('.owl-carousel'),
                    carouselNav = $this.find('.carousel-nav');

                var autoscroll = colCarousel.attr("data-autoscroll");
                if (autoscroll == 1) {
                    autoscroll = true;
                } else {
                    autoscroll = false;
                }

                var itemToShow = ($this.data('show')) ? $this.data('show') : 3;

                colCarousel.owlCarousel({
                    autoPlay: autoscroll,
                    items: itemToShow,
                    margin: 10,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true
                        },
                        600: {
                            items: 3,
                            nav: false,
                        },
                        1000: {
                            items: 5,
                            nav: true,
                            loop: false,
                        }
                    },
                    autoWidth: 'true',
                    afterAction: afterAction,
                    pagination: false
                });


                function afterAction() {
                    var $this = $(this);

                    maxItem = this.owl.owlItems.length;

                    if (maxItem <= itemToShow) {
                        carouselNav.fadeOut();
                    } else {
                        carouselNav.fadeIn();
                    }
                }


                var elPrevNavigation = $this.find('a.carousel-prev'),
                    elNextNavigation = $this.find('a.carousel-next');

                elNextNavigation.click(function(e) {
                    e.preventDefault();
                    colCarousel.trigger('owl.next');
                });

                elPrevNavigation.click(function(e) {
                    e.preventDefault();
                    colCarousel.trigger('owl.prev');
                });


                // console.log(elNavigation);

            });

            // Single Carousel
            if (singleWrapCarousel.length) {

                singleWrapCarousel.each(function(index, element) {
                    var $this = $(this),
                        colSingleCarousel = $this.find('.owl-carousel');

                    var autoscroll = colSingleCarousel.attr("data-autoscroll");
                    if (autoscroll == 1) {
                        autoscroll = true;
                    } else {
                        autoscroll = false;
                    }

                    colSingleCarousel.owlCarousel({
                        autoPlay: autoscroll,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        margin: 40,
                        autoWidth: 'true',
                        responsiveClass: true,
                        pagination: false
                    });

                    var elPrevNavigation = $this.find('a.carousel-prev'),
                        elNextNavigation = $this.find('a.carousel-next');

                    elNextNavigation.click(function(e) {
                        e.preventDefault();
                        colSingleCarousel.trigger('owl.next');
                    });

                    elPrevNavigation.click(function(e) {
                        e.preventDefault();
                        colSingleCarousel.trigger('owl.prev');
                    });

                });


            }

        }

        // responsive tables
        if ($().rtResponsiveTables) {
            $("#invoice-order").rtResponsiveTables();
        }

        // tile
        if ($('.block-tiles').length) {

            var tile = $('.tile');

            tile.each(function(index, el) {
                var imgwrapper = $(this).find('.img-wrapper'),
                    caption = $(this).find('.caption-grn'),
                    imgwrapperH = imgwrapper.height(),
                    captionH = caption.height(),
                    imgwrapperW = imgwrapper.width();

                if (captionH < imgwrapperH) {
                    setTimeout(function() {
                        caption.height(imgwrapperH);
                        caption.width(imgwrapperW);
                    }, 700)
                }
            });



            tile.hover(function() {
                var captgrn = $(this).find('.caption-grn'),
                    tileAborder = $(this).find('a');
                /* Stuff to do when the mouse enters the element */
                captgrn.css('display', 'block');
                if (Modernizr.csstransitions) {
                    captgrn.addClass('animated');

                    captgrn.removeClass('flipOutX');
                    captgrn.addClass('fadeInLeft');
                } else {
                    captgrn.fadeIn('slow');
                }

                tileAborder.css({
                    border: '0 solid #000'
                }).animate({
                    borderWidth: 5
                }, 500);

            }, function() {
                /* Stuff to do when the mouse leaves the element */
                var captgrn = $(this).find('.caption-grn'),
                    tileAborder = $(this).find('a');

                if (Modernizr.csstransitions) {
                    captgrn.removeClass('fadeInLeft');
                    captgrn.addClass('flipOutX');
                } else {
                    captgrn.fadeOut('fast');
                }

                tileAborder.animate({
                    borderWidth: 0
                }, 500);

            });
        }

        // select2 bootstrap
        if ($().select2) {
            $(".selectcustom").select2({
                minimumResultsForSearch: -1
            });
        }

        if(frmSelCountry.length){
            frmSelCountry.select2({

            });
        }

        // Avoid CLose Dropdown 
        $('body').on('click', function(e) {
            if (!$('li.dropdown.mega-dropdown').is(e.target) && $('li.dropdown.mega-dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
                $('li.dropdown.mega-dropdown').removeClass('open');
            }
        });

        if ($().nanoScroller) {
            $(".nano").nanoScroller();
        }

        // flexslider
        if ($().flexslider) {
            $('.homepage.flexslider').flexslider({
                animation: "slide",
                controlNav: false,
                directionNav: true,
            });
        }

        // Gallery

        // get the action filter option item on page load
        var $filterType = $('#filterOptions li.active a').attr('class');

        // get and assign the ourHolder element to the
        // $holder varible for use later
        var $holder = $('ul.ourHolder');

        // clone all items within the pre-assigned $holder element
        var $data = $holder.clone();

        // attempt to call Quicksand when a filter option
        // item is clicked
        $('#filterOptions li a').click(function(e) {
            // reset the active class on all the buttons
            $('#filterOptions li').removeClass('active');

            // assign the class of the clicked filter option
            // element to our $filterType variable
            var $filterType = $(this).attr('class');
            $(this).parent().addClass('active');

            if ($filterType == 'all') {
                // assign all li items to the $filteredData var when
                // the 'All' filter option is clicked
                var $filteredData = $data.find('li');
            } else {
                // find all li elements that have our required $filterType
                // values for the data-type element
                var $filteredData = $data.find('li[data-type=' + $filterType + ']');
            }

            // call quicksand and assign transition parameters
            $holder.quicksand($filteredData, {
                duration: 800,
                easing: 'easeInOutQuad'
            });
            return false;
        });


        // Fuelux
        if(fuelDiv.length){

            // init fueldiv
            if(fuelDiv.hasClass('fuelux')){
                fuelDiv.addClass('fuelux');
            }

            if($().wizard){

                // initialize reservation wizard
                console.log('start init fuelux....');
                $('#wizard-reservation').wizard();

                // data wizard validation
                $(document).on('click', '[data-wizard]', function(e) {
                    var $this = $(this),
                        href,
                        thisWizard = $(this).parents('.wizard'),
                        wizardStepContent = thisWizard.find('.step-content');
                    
                    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''));
                    var option = $this.data('wizard');
                    var item = $target.wizard('selectedItem');
                    // console.log(item);
                    var $step = wizardStepContent.find('.step-pane').eq(item.step - 1);
                    // console.log("item : "+ item.step);
                    // console.log($step);
                    // console.dir(item);
                    if ($step.find('input, select, textarea').data('required') && !$step.find('input, select, textarea').parsley('validate')) {
                        return false;
                    } else {
                        // console.log(option);
                        $target.wizard(item.step).stop();
                        var activeStep = item.step;
                        // console.log(activeStep);
                        var prev = ($(this).hasClass('btn-prev') && $(this)) || $(this).prev();
                        // console.log(prev);
                        prev.attr('disabled', (activeStep == 1) ? true : false);
                    }
                });

                function progressBarResponsive(){
                     var progressBar = fuelDivProgressBar.find('.progress-bar'),
                        calculateProgress = 0,
                        numSteps = $('#wizard-reservation').find('.steps li').length,
                        item = $('#wizard-reservation').wizard('selectedItem'),
                        activeItem = item.step;

                    var calculate = (activeItem/numSteps) * 100,
                        calc2place = calculate.toFixed(2);
                    
                    progressBar.css('width', calc2place+'%');
                }

                // onchange
                $('#wizard-reservation').on('changed.fu.wizard', function(e) {
                    progressBarResponsive();

                    e.preventDefault();
                });

                //progressbar onload
                var wizardSelect = $('#wizard-reservation').wizard('selectedItem'); 
                if(!isEmpty(wizardSelect)){
                     var progressBar = fuelDivProgressBar.find('.progress-bar'),
                        calculateProgress = 0,
                        numSteps = $('#wizard-reservation').find('.steps li').length,
                        item = wizardSelect,
                        activeItem = item.step;

                    var calculate = (activeItem/numSteps) * 100,
                        calc2place = calculate.toFixed(2);

                    console.log(activeItem);
                    
                    progressBar.css('width', calc2place+'%');
                }

            }

        }

        // Calendar Message
        $(".datepicker2").click(function(e) {
            $(".ui-datepicker-calendar").effect("pulsate", {
                times: 1
            }, 900);
            $(".calendar-notice").fadeIn(500, function() {
                // Animation complete
                 var target = $(this),
                    navbarLength = $('.navbar-fixed-top').height()

                    $('html, body').animate({
                        scrollTop: target.offset().top - navbarLength
                    }, 500, function(){
                        $(".calendar-notice").fadeOut(2500);
                    });
            });
            e.stopPropagation();
        });


        if ($().datepicker) {
            // Datepicker
            // $(".datepicker").datepicker();
            var openDatepickerID = '#open_datepicker';

            $(window).on("debouncedresize", function(event) {
                if(w.width() < 1000){
                    datepResize(openDatepickerID);
                }
            });

            // Make Datepicker Fields Read Only
            $("#open_date_from").attr('readonly', true);
            $("#open_date_to").attr('readonly', true);


            // Booking page open datepicker
                $("#open_datepicker").datepicker({
                    numberOfMonths: 2,
                    minDate: 0,
                    beforeShowDay: function(date) {
                        var date1 = openDateFrom.val();
                        var date2 = openDateto.val();
                        var fetchedData = "";
                        var dateBawaan = date;

                        // check date for data

                        var theday = (date.getMonth() + 1) + '/' +
                            date.getDate() + '/' +
                            date.getFullYear();
                        // console.log(date1);
                        // console.log(date2);

                        // console.log(theday);
                        // console.log(date1);
                        // console.log(date2);
                        fetchedData = fetchCalendarData(dateBawaan, theday, date1, date2);

                        return [true, fetchedData];
                    },
                    onSelect: function(dateText, inst) {
                        var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#open_date_from").val());
                        var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#open_date_to").val());
                        if (!date1 || date2) {
                            $("#open_date_from").val(dateText);
                            $("#open_date_to").val("");
                        } else {
                            if (Date.parse(dateText) < Date.parse(date1)) {
                                $("#open_date_from").val(dateText);
                                $("#open_date_to").val("");
                            } else {
                                $("#open_date_to").val(dateText);
                            }
                        }

                        // updateDatePickerCellsData();
                        datepResize(openDatepickerID);
                    },
                    beforeShow: function(elem, dp) { //This is for non-inline datepicker
                        // updateDatePickerCellsData();
                        datepResize(openDatepickerID);
                    },
                    onChangeMonthYear: function(month, year, dp) {
                        // updateDatePickerCellsData();
                        datepResize(openDatepickerID);
                    },
                });
                // updateDatePickerCellsDopata();
                datepResize(openDatepickerID);
            }


        // Data Toggle 
        if(togglehvr.length){
            togglehvr.on('click', function(event) {
                event.preventDefault();
                
                var $collapse = $(this).next('#caption-hvr'),
                    $_this = $(this);
                // console.log($collapse);
                $collapse.collapse('toggle');

                $collapse.on('shown.bs.collapse', function(){
                    $_this.find('.iconplus-add139').addClass('iconplus-minus87').removeClass('iconplus-add139');
                }).on('hidden.bs.collapse', function(){
                    $_this.find('.iconplus-minus87').addClass('iconplus-add139').removeClass('iconplus-minus87');
                });
            });

            $('body').on('click', function(e){
                var target = $(e.target),
                    $collapse = togglehvr.next('#caption-hvr.in');

                    // console.log($collapse);
                if(!target.parents('.img-product').length){
                    $collapse.collapse('toggle');
                }
            });
        }

        // gallery product
        if($('#gallery-product').length){
            var linkBlock = $('.img-block'),
                mTop = 0;

            // console.log(linkBlock)
            linkBlock.hover(function() {

                    var $this = $(this);

                    var fromTop = ($('.img-wrapper', $this).height() / 2 - $('.caption-block', $this).height() / 2);

                    
                    $('.caption-block', $this).css('height', ($('.img-wrapper', this).height()));
                    $('.caption-block', $this).css('margin-top', 0);
                    

                    $('.mediaCaption', $this).height($('.img-wrapper', $this).height());

                    $('.mask', this).css('height', $('.img-wrapper', this).height());
                    $('.mask', this).css('width', $('.img-wrapper', this).width());
                    $('.mask', this).css('margin-top', $('.img-wrapper', this).height());


                    $('.mask', this).stop(1).show().css('margin-top', $('.img-wrapper', this).height()).animate({
                        marginTop: mTop
                    }, 100, function() {

                        $('.caption-block', $this).css('display', 'block');
                        if (Modernizr.csstransitions) {
                            $('.caption-block a').addClass('animated animatedVisi' );
                            $('.caption-block h1').addClass('animated animatedVisi');
                            $('.caption-block p').addClass('animated animatedVisi');


                            $('.caption-block h1', $this).removeClass('fadeOut');
                            $('.caption-block h1', $this).addClass('fadeInDown');
                            $('.caption-block p', $this).removeClass('fadeOut');
                            $('.caption-block p', $this).addClass('fadeInUp');
                            $('.caption-block a', $this).removeClass('flipOutX');
                            $('.caption-block a', $this).addClass('fadeInDown');

                        } else {

                            $('.caption-block', $this).stop(true, false).fadeIn('fast');
                        }

                    });
                
            }, function() {
                    var $this = $(this);


                    $('.mask', this).stop(1).show().animate({
                        marginTop: $('.img-wrapper', $this).height()
                    }, 200, function() {

                        if (Modernizr.csstransitions) {
                            $('.caption-block a', $this).removeClass('fadeInDown');
                            $('.caption-block a', $this).addClass('flipOutX');

                            $('.caption-block h1', $this).removeClass('fadeInDown');
                            $('.caption-block h1', $this).addClass('fadeOut');
                            $('.caption-block p', $this).removeClass('fadeInUp');
                            $('.caption-block p', $this).addClass('fadeOut');


                        } else {
                            $('.caption-block', $this).stop(true, false).fadeOut('fast');
                        }

                    });
            });
        }


        // progressbar based on list width 
        if((progressContainer.length) && (wizardUlSteps.length)){
            var pgWidth = progressContainer.width(),
                ulstepsWidth = wizardUlSteps.width(), 
                finalWidth = ulstepsWidth - 20,
                progressHolder = progressContainer.parent('.progress-holder');

            if(pgWidth > ulstepsWidth){
                progressHolder.width(finalWidth);
            }

            $(window).on("debouncedresize", function(event) {
                if(w.width() < 767){
                var progressHolder = $('.progress-holder');
                    progressHolder.removeAttr('style');
                }else{
                    var pgWidth = progressContainer.width(),
                        ulstepsWidth = wizardUlSteps.width(), 
                        finalWidth = ulstepsWidth - 20,
                        progressHolder = progressContainer.parent('.progress-holder');

                    if(pgWidth > ulstepsWidth){
                        progressHolder.width(finalWidth);
                    }
                }
            });
        }

        // isotope 
        if($('.isotope').length){
            $('.isotope').isotope({
              // options...
              itemSelector: '.isotope-item'
            });
        }

        // vertical center
        if($().flexVerticalCenter){
            if($('.content-caption').length){
                $('.content-caption').each(function(index, el) {
                    var parentor = $(this).parents('.caption-col')
                    $(this).flexVerticalCenter({
                        parentSelector : '.caption-col',
                    }); 
                    // console.log(parentor[0]);
                    // console.log($(this));
                });
            }
        }

        // setting Float Form reservation
        if(floatForm.length){
            var FRTCHeight = FRTopCol.height(),
                FRBCHeight = FRBottomCol.height(),
                linkReservation = $('.bc a'),
                toggleBlock = $('span.blocktoggle'),
                totalHeight = FRTCHeight + FRBCHeight;

            contentFR.animate({
                'top' : '-'+FRTCHeight+'px',
                'display': 'block'},
            1000);

            linkReservation.click(function(e) {
                e.preventDefault();
                var $this = $(this);

                if($(this).is('.open')){
                    contentFR.animate({
                        'top' : '-'+FRTCHeight+'px'}
                    ,500);

                    $this.removeClass('open');
                }else{
                    contentFR.animate({
                        'top' : '-'+totalHeight+'px'},
                    500);

                    $(this).addClass('open');
                }
            });

            $('span:not(.blocktoggle)').click(function(e) {
                var $target = $(e.target);
                e.preventDefault();
                if(linkReservation.hasClass('open') && !$target.is(FRBottomCol.find('*')) && ($('.ui-datepicker').css('display') == 'none') ){
                    contentFR.animate({
                            'top' : '-'+FRTCHeight+'px'}
                    ,500);

                    linkReservation.removeClass('open');
                }
                console.log('divnot yg ditrigger');
                console.log($('.ui-datepicker').css('display'));
            });

            $(document).on("click", function(e){
                var $target = $(e.target);
                if(!$target.is("span.blocktoggle") && linkReservation.hasClass('open') && !$target.is(FRBottomCol.children()) && !$target.is(FRBottomCol.find('*')) && ($('.ui-datepicker').css('display') == 'none')){
                    contentFR.animate({
                        'top' : '-'+FRTCHeight+'px'}
                    ,500);

                linkReservation.removeClass('open');
                console.log($target);
                console.log(FRBottomCol.children());
                }
            });
        }


    });

})(jQuery);


function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

// Update Datepicker based on Data 
function updateDatePickerCellsData(){
    setTimeout(function () {
        //Fill this with the data you want to insert (I use and AJAX request).  Key is day of month
        //NOTE* watch out for CSS special characters in the value
        var cellContents = {1: '20', 15: '60', 28: '100'};

        //Select disabled days (span) for proper indexing but // apply the rule only to enabled days(a)
        $('.ui-datepicker td > *').each(function (idx, elem) {
            var value = cellContents[idx + 1] || 0;

            // dynamically create a css rule to add the contents //with the :after                         
  //             selector so we don't break the datepicker //functionality 
            var className = 'datepicker-content-' + value;

            if(value == 0)
                addCSSRule('.ui-datepicker td a.' + className + ':after {content: "\\a0";}'); //&nbsp;
            else
                addCSSRule('.ui-datepicker td a.' + className + ':after {content: "' + value + '";}');

            // $(this).addClass(className);
        });
    }, 0);
}

function fetchCalendarData(dateBawaan, date, date1, date2) {
    var theday = date,
        classAdd = "",
        date1 = (date1 != "") ? $.datepicker.parseDate($.datepicker._defaults.dateFormat, date1) : '',
        date2 = (date2 != "") ? $.datepicker.parseDate($.datepicker._defaults.dateFormat, date2) : '',
        dateBw = dateBawaan;


    var dates = [new Date(2014, 9 - 1, 1), new Date(2014, 9 - 1, 9)];
    var datesAvailable = ['12/5/2014', '12/9/2014'];
    var datesRestricted = ['12/1/2014', '12/17/2014'];
    var datesBooked = ['10/1/2014', '12/17/2014', '12/10/2014'];

    // console.log(datesAvailable);
    // console.log(date1);
    // console.log(date2);
    // console.log("-------------------");
    // console.log('d1 : '+date1+', d2 : '+date2)
    // if (theday.length) {
    //     if (datesAvailable.length) {
    //         classAdd += $.inArray(theday, datesAvailable) >= 0 ? "available " : "";
    //     }
    //     if (datesRestricted.length) {
    //         classAdd += $.inArray(theday, datesRestricted) >= 0 ? "restricted " : "";
    //     }
    //     if (datesBooked.length) {
    //         classAdd += $.inArray(theday, datesBooked) >= 0 ? "booked " : '';
    //     }
    // }

    // console.log(date1+" + "+date2);
    if (date1 != '' || date2 != '') {
        classAdd += date1 && ((dateBw.getTime() == date1.getTime()) || (date2 && dateBw >= date1 && dateBw <= date2)) ? " dp-highlight " : "";
    }

    // console.log(classAdd);
    return classAdd;

}

var dynamicCSSRules = [];
function addCSSRule(rule) {
    if ($.inArray(rule, dynamicCSSRules) == -1) {
        $('head').append('<style>' + rule + '</style>');
        dynamicCSSRules.push(rule);
    }
}

function datepResize(idCal) {

    // Get width of parent container
    console.log(idCal);
    var width = jQuery(idCal).width(); //attr('clientWidth');
    if (width == null || width < 1){
        // For IE, revert to offsetWidth if necessary
        width = jQuery(idCal).attr('offsetWidth');
    }

    width = width - 2; // Fudge factor to prevent horizontal scrollbars
    if (width > 0 &&
        // Only resize if new width exceeds a minimal threshold
        // Fixes IE issue with in-place resizing when mousing-over frame bars
        Math.abs(width - jQuery("div ui-datepicker").width()) > 5)
    {
        if (width < 166){
            jQuery("div.ui-datepicker").css({'font-size': '8px'});
            jQuery(".ui-datepicker td .ui-state-default").css({'padding':'0px','font-size': '6px'});
        }
        else if (width > 328){
            jQuery("div.ui-datepicker").css({'font-size': '13px'});
            jQuery(".ui-datepicker td .ui-state-default").css({'padding':'10px','font-size': '100%'});

        }
        else{
            jQuery("div.ui-datepicker").css({'font-size': (width-43)/16+'px'});
        }
    }

}